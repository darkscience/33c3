33C3 is the upcoming 33rd manifestation of the Chaos Communication Congress.
Several of the Darkscience members are planning to attend the congress to represent the network and enjoy the various activities.

This repository covers any organisational necessities for this event.

* [people](https://git.drk.sc/darkscience/33c3/wikis/attendees)
* [stuff](https://git.drk.sc/darkscience/33c3/wikis/bringthings)
* [swag](https://git.drk.sc/darkscience/33c3/wikis/swag)
* ideas on how to grow the community

Please check the [wiki](https://git.drk.sc/darkscience/33c3/wikis/home), since the bulk of cogent information will be there.